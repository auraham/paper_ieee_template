# Template for IEEE Transactions on Evolutionary Computation

This repository contains the Latex source for this journal.

**Compilation**

```
pdflatex -shell-escape paper_v1.tex
bibtex paper_v1.aux
pdflatex -shell-escape paper_v1.tex
pdflatex -shell-escape paper_v1.tex
```



## Contact

Auraham Camacho `auraham.cg@gmail.com`